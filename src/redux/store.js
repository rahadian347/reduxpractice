import {createStore, combineReducers} from 'redux'
import appReducers from './reducer/'


const configureStore = () => {
    return createStore(appReducers)
}

export default configureStore
